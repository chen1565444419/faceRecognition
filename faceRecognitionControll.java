package com.zfsoft.artificialIntelligence.faceRecognition.controller;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.zfsoft.artificialIntelligence.faceRecognition.client.FaceDetect;
import com.zfsoft.artificialIntelligence.faceRecognition.client.FaceMatch;
import com.zfsoft.base.BaseController;
import com.zfsoft.util.JsonUtil;

/**
 * 人脸识别服务 controller
 * @author liyingming
 *
 */
@Controller
@RequestMapping(value = "/faceRecognition")
public class faceRecognitionController extends BaseController {

	 /**
	 * 人脸检测测试页面
	 * @return
	 * @throws Exception  
	 */
    @RequestMapping(value = "/test.do")
    public ModelAndView queryVoi() throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/artificialIntelligence/faceRecognition/test");
        return modelAndView;
    }
	
    /**
   	 * 请求人脸检测
   	 * @return
   	 * @throws Exception  
   	 */
	@RequestMapping(value = "/save.do")
	@ResponseBody
	public Map<String, Object> queryService(@RequestParam("the_file") MultipartFile file) {
		Map<String, Object> modelMap = new HashMap<String, Object>();
		try {
			//将数据转为流
			InputStream content = file.getInputStream();
			ByteArrayOutputStream swapStream = new ByteArrayOutputStream();  
	        byte[] buff = new byte[100];  
	        int rc = 0;  
	        while ((rc = content.read(buff, 0, 100)) > 0) {  
	            swapStream.write(buff, 0, rc);  
	        }  
	        //获得二进制数组
	        byte[] in2b = swapStream.toByteArray(); 
	        //调用人脸检测的方法
	        Map<String, String>  strmap = FaceDetect.detectby(in2b);
	        //将map数据封装json传到前台
			String strjson = JsonUtil.mapToJson(strmap);
			modelMap.put("strjson", strjson);
			modelMap.put("success", true);
		} catch (Exception e) {
			modelMap.put("success", false);
			modelMap.put("data", e.getMessage());
		}
		return modelMap;
	}
	
	
}
